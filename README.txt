CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module provides a predefined and configurable block which can be 
easily enabled and disabled in case of a crisis or any other 
situation which needs immediate communication.

REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/docs/8/extending-drupal-8/installing-modules
   for further information.


CONFIGURATION
-------------

 * Configure the module settings in 	
   Configuration / System / Crisis Mode settings


MAINTAINERS
-----------

Current maintainers:
 * Daniel Lanz (dalanz) - https://drupal.org/u/dalanz
